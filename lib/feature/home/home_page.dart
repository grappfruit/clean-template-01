import 'package:cleantemplate01/localization/local_localizations.dart';
import 'package:cleantemplate01/theme/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final localizations = LocalLocalizations.of(context);
    final appTheme = Provider.of<AppTheme>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(
          localizations.appTitle,
          style: appTheme.appBarTitle,
        ),
        backgroundColor: appTheme.primary,
      ),
      body: Text("home page"),
    );
  }
}
