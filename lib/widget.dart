import 'package:cleantemplate01/globals.dart';
import 'package:cleantemplate01/localization/local_localizations.dart';
import 'package:flutter/material.dart';

class ErrorScreen extends StatelessWidget {
  final VoidCallback onRetry;

  const ErrorScreen({@required this.onRetry, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final localizations = LocalLocalizations.of(context);

    return Scaffold(
      body: Column(
        children: <Widget>[
          Text(localizations.splashScreenErrorMessage),
          FlatButton(
            child: Text(localizations.retry),
            onPressed: () => appRestart(),
          ),
        ],
      ),
    );
  }
}
