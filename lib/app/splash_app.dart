import 'package:cleantemplate01/globals.dart';
import 'package:cleantemplate01/localization/local_localization_delegate.dart';
import 'package:cleantemplate01/locator/locator.dart';
import 'package:cleantemplate01/theme/app_theme_provider.dart';
import 'package:cleantemplate01/widget.dart';
import 'package:fimber/fimber.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class SplashApp extends StatelessWidget {
  final VoidCallback onInitializationSuccessful;

  const SplashApp({
    @required this.onInitializationSuccessful,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      builder: (context, child) => AppThemeProvider(
        child: child,
      ),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        LocalLocalizationDelegate(),
      ],
      supportedLocales: const [Locale('de', 'DE')],
      home: _SplashWidget(
        onInitializationSuccessful: onInitializationSuccessful,
      ),
    );
  }
}

class _SplashWidget extends StatefulWidget {
  final VoidCallback onInitializationSuccessful;

  const _SplashWidget({
    @required this.onInitializationSuccessful,
    Key key,
  }) : super(key: key);

  @override
  _SplashWidgetState createState() => _SplashWidgetState();
}

class _SplashWidgetState extends State<_SplashWidget> {
  bool _hasError = false;

  @override
  void initState() {
    super.initState();
    _initApp();
  }

  Future<void> _initApp() async {
    try {
      await _initAsyncDependencies(); // has to be first
      widget.onInitializationSuccessful();
    } catch (ex) {
      Fimber.e('error during initialization of the app', ex: ex);
      _setError();
    }
  }

  Future<void> _initAsyncDependencies() async {
    await initAsyncDependencies();
  }

  void _setError() {
    setState(() {
      _hasError = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_hasError) {
      return ErrorScreen(
        onRetry: () => appRestart,
      );
    }
    return const CircularProgressIndicator();
  }
}
