import 'package:cleantemplate01/feature/home/home_page.dart';
import 'package:cleantemplate01/localization/local_localization_delegate.dart';
import 'package:cleantemplate01/theme/app_theme_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class MainApp extends StatelessWidget {
  const MainApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      builder: (context, child) => AppThemeProvider(
        child: child,
      ),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        LocalLocalizationDelegate(),
      ],
      supportedLocales: const [Locale('de', 'DE')],
      home: HomePage(),
    );
  }
}
