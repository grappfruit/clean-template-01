// Globals we need on other places. For example the function to restart the
// app. Different flavors have to register the correct function here.

import 'package:cleantemplate01/config/build_config.dart';
import 'package:cleantemplate01/theme/app_theme.dart';
import 'package:fimber/fimber.dart';

CiColors ciColors;
Function appRestart;

void initCiColors(CiColors colors) {
  ciColors = colors;
}

void initAppRestartFunction(Function function) {
  appRestart = function;
}

void initLogging() {
  if (isDebug) {
    Fimber.plantTree(DebugTree());
  }
}
