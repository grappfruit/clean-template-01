import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get_it/get_it.dart';

GetIt l = GetIt.instance;

void resetDependencies() {
  l.reset();
}

void initSyncDependencies() {
  _initFirebase();
  _initMapper();
  _initRepositories();
  _initViewModels();
  _initUseCases();
}

void _initFirebase() {
  l.registerLazySingleton(() => Firestore.instance);
}

void _initMapper() {}

void _initRepositories() {}

void _initViewModels() {}

void _initUseCases() {}

Future<void> initAsyncDependencies() async {}
