import 'dart:async';

import 'package:flutter/material.dart';

class LocalLocalizations {
  LocalLocalizations(this.locale);

  final Locale locale;
  final Map<Locale, Map<String, dynamic>> translations =
      <Locale, Map<String, String>>{
    const Locale('de', 'DE'): <String, String>{
      'appTitle': 'MealPro Staff App',
      'splashScreenErrorMessage': 'Fehler beim initialisieren der App',
      'retry': 'Erneut versuchen',
    },
  };

  static LocalLocalizations of(BuildContext context) {
    return Localizations.of<LocalLocalizations>(context, LocalLocalizations);
  }

  Future<bool> load() async {
    return true;
  }

  String _trans(String key) {
    return translations[locale][key] as String ?? '__${key.toUpperCase()}__';
  }

  String get appTitle => _trans('appTitle');

  String get splashScreenErrorMessage => _trans('splashScreenErrorMessage');

  String get retry => _trans('retry');
}
