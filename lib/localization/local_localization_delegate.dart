import 'dart:async';

import 'package:cleantemplate01/localization/local_localizations.dart';
import 'package:flutter/material.dart';

class LocalLocalizationDelegate
    extends LocalizationsDelegate<LocalLocalizations> {
  LocalLocalizationDelegate();

  @override
  bool isSupported(Locale locale) => true;

  @override
  Future<LocalLocalizations> load(Locale locale) async {
    return LocalLocalizations(locale);
  }

  @override
  bool shouldReload(LocalizationsDelegate old) => false;
}
