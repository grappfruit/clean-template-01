import 'package:cleantemplate01/app/main_app.dart';
import 'package:cleantemplate01/app/splash_app.dart';
import 'package:cleantemplate01/globals.dart';
import 'package:cleantemplate01/locator/locator.dart';
import 'package:cleantemplate01/theme/app_theme.dart';
import 'package:flutter/material.dart';

void main() {
  initLogging();
  initCiColors(CiColors(
    const Color(0xFF07B2C5),
    const Color(0xFFA6E9EF),
    const Color(0xFF014950),
    const Color(0xFFFFAF06),
  ));
  initAppRestartFunction(runMainApp);
  resetDependencies();
  initSyncDependencies();
  runApp(SplashApp(
    key: UniqueKey(),
    onInitializationSuccessful: runMainApp,
  ));
}

void runMainApp() {
  runApp(MainApp(
    key: UniqueKey(),
  ));
}
