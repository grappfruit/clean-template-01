import 'package:cleantemplate01/globals.dart';
import 'package:cleantemplate01/theme/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

const _iPhoneSEWidth = 320.0;
const _iPadWidth = 768.0;

class AppThemeProvider extends StatefulWidget {
  final Widget child;

  const AppThemeProvider({@required this.child, Key key}) : super(key: key);

  @override
  _AppThemeProviderState createState() => _AppThemeProviderState();
}

class _AppThemeProviderState extends State<AppThemeProvider> {
  AppTheme appTheme;
  SystemUiOverlayStyle _systemUiOverlayStyle;

  @override
  void didChangeDependencies() {
    final screenWidth = MediaQuery.of(context).size.width;
    final brightness = MediaQuery.of(context).platformBrightness;

    ScreenSize size;
    if (screenWidth <= _iPhoneSEWidth) {
      size = ScreenSize.small;
    } else if (screenWidth >= _iPadWidth) {
      size = ScreenSize.large;
    } else {
      size = ScreenSize.medium;
    }

    if (ciColors == null) {
      throw Exception('ciColors not initialized');
    }
    appTheme = AppTheme(size, ciColors);

    _systemUiOverlayStyle = SystemUiOverlayStyle(
      systemNavigationBarColor: brightness == Brightness.dark
          ? appTheme.greyLvl3
          : appTheme.white, // navigation bar color// status bar color
      systemNavigationBarIconBrightness:
          brightness == Brightness.dark ? Brightness.light : Brightness.dark,
    );

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    // override material theme as well
    return AnnotatedRegion<SystemUiOverlayStyle>(
      child: AnimatedTheme(
        isMaterialAppTheme: true,
        data: appTheme.materialThemeData,
        child: Provider<AppTheme>.value(
          value: appTheme,
          child: widget.child,
        ),
      ),
      value: _systemUiOverlayStyle,
    );
  }
}
