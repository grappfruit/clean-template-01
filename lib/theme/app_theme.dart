import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

enum ScreenSize { small, medium, large }

class CiColors {
  /// should be a dark color as all themes are light
  final Color primary;
  final Color primaryLight;
  final Color primaryDark;

  /// should be a dark color as all themes are light
  final Color accent;

  CiColors(this.primary, this.primaryLight, this.primaryDark, this.accent);
}

class AppTheme {
  double _grid;

  double get padding1_2 => _grid / 2;

  double get padding => _grid;

  double get padding2X => _grid * 2;

  double get padding3X => _grid * 3;

  double get padding4X => _grid * 4;

  double get padding5X => _grid * 5;

  final CiColors _ciColors;

  Color get primary => _ciColors.primary;

  Color get primaryLight => _ciColors.primaryLight;

  Color get primaryDark => _ciColors.primaryDark;

  Color get accent => _ciColors.accent;

  MaterialColor get primaryMaterial =>
      MaterialColor(primary.value, <int, Color>{
        50: primary,
        100: primary,
        200: primary,
        300: primary,
        400: primary,
        500: primary,
        600: primary,
        700: primary,
        800: primary,
        900: primary,
      });
  Color black = const Color(0xFF000000);
  Color white = const Color(0xFFFFFFFF);
  Color greyLvl1 = const Color(0xFFF4F4F4);
  Color greyLvl2 = const Color(0xFFCFCBCA);
  Color greyLvl3 = const Color(0xFF918E8C);
  Color greyLvl4 = const Color(0xFF494745);
  Color greyLvl5 = const Color(0xFF272422);
  Color shadow = const Color(0x26000000);
  Color success = const Color(0xFF70C118);
  Color alert = const Color(0xFFFFA500);
  Color registerFormAlert = const Color(0xFFFFF000);
  Color error = const Color(0xFFFF0000);
  Color info = const Color(0xFF009FE3);
  Color background = const Color(0xFFFFFFFF);

  TextStyle get appBarTitle => TextStyle(
        color: white,
      );

  ThemeData get materialThemeData => ThemeData(
        appBarTheme: const AppBarTheme(
          brightness: Brightness.light,
          color: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
          elevation: 1.0,
        ),
        iconTheme: IconThemeData(color: greyLvl3),
        inputDecorationTheme: InputDecorationTheme(
          labelStyle: TextStyle(color: greyLvl3),
          helperStyle: TextStyle(color: greyLvl3),
          hintStyle: TextStyle(color: greyLvl3),
          suffixStyle: TextStyle(color: black),
          errorBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: error, width: 2.0)),
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: greyLvl3, width: 2.0)),
          contentPadding: EdgeInsets.zero,
        ),
        scaffoldBackgroundColor: Colors.white,
        primarySwatch: primaryMaterial,
        bottomSheetTheme: BottomSheetThemeData(
          backgroundColor: Colors.transparent,
          modalBackgroundColor: white,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              top: Radius.circular(20),
            ),
          ),
        ),
      );

  AppTheme(ScreenSize screenSize, this._ciColors) {
    switch (screenSize) {
      case ScreenSize.small:
        _grid = 6.0;
        break;
      case ScreenSize.medium:
        _grid = 8.0;
        break;
      case ScreenSize.large:
        _grid = 16.0;
        break;
    }
  }
}
